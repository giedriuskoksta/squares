import { Component } from '@angular/core';

@Component({
  selector: 'app-nav',
  template: `
    <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top">
      <ul class="nav navbar-nav mr-auto">
        <li class="nav-item" routerLinkActive="active">
          <a class="nav-link" routerLink="/lists">Point Lists</a>
        </li>
      </ul>
    </nav>
  `
})
export class NavigationComponent {}
