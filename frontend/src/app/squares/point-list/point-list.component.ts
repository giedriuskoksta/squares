import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {SquaresService} from '../shared/squares.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PointList} from '../shared/point-list.model';

@Component({
  selector: 'squares-point-list',
  templateUrl: './point-list.component.html',
  styleUrls: ['./point-list.component.scss']
})

export class PointListComponent implements OnInit {

  private selectedPointListId: number;
  private pointList: PointList;
  private needsSquareRefresh = true;
  private needsPointsRefresh = true;

  public constructor(
    private titleService: Title,
    private route: ActivatedRoute,
    private router: Router,
    private squaresService: SquaresService
  ) {}

  ngOnInit(): void {
    this.selectedPointListId = +this.route.snapshot.params['listId'];
    this.getPointList();
  }

  getPointList(): void {
    this.squaresService.getPointList(this.selectedPointListId)
      .then(pointList => {
        this.pointList = pointList;
        this.titleService.setTitle('List - ' + this.pointList.name);
      });
  }

  onGoBack(): void {
    this.router.navigate(['/lists']);
  }

  onPointDelete(): void {
    this.needsSquareRefresh = true;
  }

  onSquaresUpdated(): void {
    this.needsSquareRefresh = false;
  }

  onPointsUpdated(): void {
    this.needsPointsRefresh = false;
  }

  onPointAdded(): void {
    this.needsSquareRefresh = true;
    this.needsPointsRefresh = true;
  }
}

