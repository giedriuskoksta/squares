import {Component, Input, EventEmitter, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SquaresService} from '../shared/squares.service';

@Component({
  selector: 'squares-new-point-list-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Start new list</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Dismiss')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group {{ validation.length > 0 ? 'has-danger' : '' }}">
        <input type="text" [(ngModel)]="newListName" name="newListName" class="form-control" id="newListName" placeholder="Enter list name">
        <div ng-if="validation != ''" class="form-control-feedback">{{ validation }}</div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-success" (click)="addPoint()">Save</button>
    </div>
    `
})
export class NewPointListContent {
  @Input() notifyAdded: EventEmitter<string>;
  @Input() newListName: string;
  validation: string[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private squaresService: SquaresService) {}

  addPoint(): void {
    this.validation = [];
    if (this.newListName == null) {
      this.validation.push('Please enter name');
    } else {
      this.squaresService
        .startNewList(this.newListName)
        .then(() => {
          this.activeModal.close(true);
          this.notifyAdded.emit('POINT_ADDED');
        });
    }
  }
}

@Component({
  selector: 'squares-new-point-list',
  templateUrl: './new-point-list.component.html'
})

export class NewPointListComponent {
  @Output() notifyAdded: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private modalService: NgbModal
  ) {}

  open(): void {
    const modalRef = this.modalService.open(NewPointListContent);
    modalRef.componentInstance.notifyAdded = this.notifyAdded;
  }
}
