import {Component, Input, OnChanges, EventEmitter, Output} from '@angular/core';
import {SquaresService} from '../shared/squares.service';
import {PointList} from '../shared/point-list.model';
import {Square} from '../shared/square.model';

@Component({
  selector: 'squares-detected-squares',
  templateUrl: './detected-squares.component.html'
})

export class DetectedSquaresComponent implements OnChanges {

  currentPage = 1;
  perPage = 5;
  totalSquares: number;

  @Input() selectedPointList: PointList;
  @Input() needsSquareRefresh: boolean;
  private squares: Square[] = [];
  @Output() notifyUpdate: EventEmitter<string> = new EventEmitter<string>();

  public constructor(
    private squaresService: SquaresService
  ) {}

  ngOnChanges() {
    if (this.needsSquareRefresh === true) {
     this.getSquares();
    }
  }

  getSquares(): void {
    this.squaresService.getSquares(this.selectedPointList, this.currentPage, this.perPage).then(squaresResponse => {
      this.squares = squaresResponse.items;
      this.totalSquares = squaresResponse.total;
      this.notifyUpdate.emit('SQUARES_UPDATED');
    });
  }

  onPaginationChange(): void {
    this.getSquares();
  }
}

