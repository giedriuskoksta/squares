import {Component, Input, Output, EventEmitter} from '@angular/core';
import {PointList} from '../shared/point-list.model';
import {SquaresService} from '../shared/squares.service';

@Component({
  selector: 'squares-point-list-name-change',
  templateUrl: './point-list-name-change.component.html'
})

export class PointListNameChangeComponent {
  @Input() selectedPointList: PointList;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  public constructor(
    private squaresService: SquaresService
  ) {}

  rename(): void {
    this.squaresService
      .updatePointList(this.selectedPointList)
      .then(() => {
        this.notify.emit('RENAME_DONE');
      });
  }
}
