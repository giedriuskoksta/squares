import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PointListsComponent} from './point-lists/point-lists.component';
import {PointListComponent} from './point-list/point-list.component';

const squaresRoutes: Routes = [
  { path: 'lists', component: PointListsComponent},
  { path: 'lists/:listId', component: PointListComponent}
];


@NgModule({
  imports: [
    RouterModule.forChild(squaresRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class SquaresRoutingModule {
}
