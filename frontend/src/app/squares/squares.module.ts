import {NgModule} from '@angular/core';
import {PointListsComponent} from './point-lists/point-lists.component';
import {SquaresRoutingModule} from './squares-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {SquaresService} from './shared/squares.service';
import {PointListNameChangeComponent} from './point-list-name-change/point-list-name-change.component';
import {FormsModule} from '@angular/forms';
import {PointListComponent} from './point-list/point-list.component';
import {PointsComponent} from './points/points.component';
import {DetectedSquaresComponent} from './detected-squares/detected-squares.component';
import {NewPointListComponent, NewPointListContent} from './new-point-list/new-point-list.component';
import {NewPointComponent, NewPointContent} from './new-point/new-point.component';
import {ImportPointsContent, ImportPointsComponent} from './import-points/import-points.component';
import {FileSelectDirective} from 'ng2-file-upload';
import {NgbButtonsModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {PerPageComponent} from './per-page/per-page.component';

@NgModule({
  imports: [
    SquaresRoutingModule,
    BrowserModule,
    FormsModule,
    NgbPaginationModule,
    NgbButtonsModule
  ],
  declarations: [
    PointListsComponent,
    PointListNameChangeComponent,
    PointListComponent,
    PointsComponent,
    DetectedSquaresComponent,
    NewPointListComponent,
    NewPointListContent,
    NewPointComponent,
    NewPointContent,
    ImportPointsComponent,
    ImportPointsContent,
    FileSelectDirective,
    PerPageComponent
  ],
  providers: [
    SquaresService
  ],
  exports: [],
  entryComponents: [
    NewPointListContent,
    NewPointContent,
    ImportPointsContent
  ],
  bootstrap: [PointsComponent]
})

export class SquaresModule {
}
