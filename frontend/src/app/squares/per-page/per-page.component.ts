import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'squares-per-page',
  templateUrl: './per-page.component.html'
})

export class PerPageComponent {

  @Input() perPage = 5;
  @Output() perPageChange: EventEmitter<number> = new EventEmitter<number>();

  changePerPage(newPerPage): void {
    if (newPerPage !== this.perPage) {
      this.perPage = newPerPage;
      this.perPageChange.emit(newPerPage);
    }
  }
}

