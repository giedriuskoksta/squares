import {Component, Input, EventEmitter, Output, OnChanges} from '@angular/core';
import {SquaresService} from '../shared/squares.service';
import {Point} from '../shared/point.model';
import {PointList} from '../shared/point-list.model';

@Component({
  selector: 'squares-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.scss']
})

export class PointsComponent implements OnChanges {

  currentPage = 1;
  perPage = 5;
  totalPoints: number;
  @Input() selectedPointList: PointList;
  @Input() needsPointsRefresh: boolean;
  private points: Point[] = [];
  @Output() notifyDelete: EventEmitter<string> = new EventEmitter<string>();
  @Output() notifyUpdate: EventEmitter<string> = new EventEmitter<string>();

  public constructor(
    private squaresService: SquaresService
  ) {}

  ngOnChanges() {
    if (this.needsPointsRefresh === true) {
      this.getPoints();
    }
  }

  getPoints(): void {
    this.squaresService.getPoints(this.selectedPointList, this.currentPage, this.perPage).then(pointListResponse => {
      this.points = pointListResponse.items;
      this.totalPoints = pointListResponse.total;
      this.notifyUpdate.emit('POINTS_UPDATED');
    });
  }
  delete(pointToDelete: Point): void {
    this.squaresService.deletePoint(this.selectedPointList, pointToDelete)
      .then(() => {
        this.points = this.points.filter(p => p !== pointToDelete);
        this.getPoints();
        this.notifyDelete.emit('POINT_DELETED');
      })
  }

  onPaginationChange(): void {
    this.getPoints();
  }

  exportPage(): void {
    const csv = this.points.map(function(d){
      return d.x + ' ' + d.y;
    }).join('\n').replace(/(^\[)|(\]$)/mg, '');

    const blob = new Blob([csv], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
}

