import {Component, Input, EventEmitter, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SquaresService} from '../shared/squares.service';
import {PointList} from '../shared/point-list.model';
import {Point} from '../shared/point.model';

@Component({
  selector: 'squares-new-point-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Add new Point</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Dismiss')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group {{ validation.length > 0 ? 'has-danger' : '' }}">
        <div class="row">
          <div class="col">
            <input type="text" [(ngModel)]="x" name="x" class="form-control" id="x" placeholder="X">
          </div>
          <div class="col">
            <input type="text" [(ngModel)]="y" name="y" class="form-control" id="y" placeholder="Y">
          </div>
        </div>
        <div ng-if="validation == ''" class="form-control-feedback">
          <p *ngFor="let message of validation">{{ message }}</p>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-success" (click)="addPoint()">Save</button>
    </div>
  `
})

export class NewPointContent {
  @Input() pointList: PointList;
  @Input() notifyAdded: EventEmitter<string>;
  @Input() x: number;
  @Input() y: number;
  private validation: string[] = [];

  constructor(
    public activeModal: NgbActiveModal,
    private squaresService: SquaresService) {}

  addPoint(): void {
    this.validation = [];
    if (this.x == null || this.y == null) {
      this.validation.push('Please enter name');
    } else {
      this.squaresService
        .addNewPoint(this.pointList, new Point(this.x, this.y))
        .then((response) => {
          if (response.length > 0) {
            this.validation = response;
          } else {
            this.activeModal.close(true);
            this.notifyAdded.emit('POINT_ADDED');
          }
        });
    }
  }
}

@Component({
  selector: 'squares-new-point',
  templateUrl: './new-point.component.html'
})
export class NewPointComponent {
  @Input() pointList: PointList;
  @Output() notifyAdded: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private modalService: NgbModal
  ) {}

  open(): void {
    const modalRef = this.modalService.open(NewPointContent);
    modalRef.componentInstance.pointList = this.pointList;
    modalRef.componentInstance.notifyAdded = this.notifyAdded;
  }
}
