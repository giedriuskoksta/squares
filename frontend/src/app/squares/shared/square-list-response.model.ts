import {Square} from './square.model';

export class SquareListResponse {
  constructor(public total: number,
              public items: Square[]) {
  }
}
