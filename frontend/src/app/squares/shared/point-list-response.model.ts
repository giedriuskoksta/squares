import {Point} from './point.model';

export class PointListResponse {
  constructor(public total: number,
              public items: Point[]) {
  }
}
