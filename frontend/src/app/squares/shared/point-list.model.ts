import {Point} from './point.model';

export class PointList {
  constructor(public id: number,
              public name: string,
              public points: Point[]) {
  }
}
