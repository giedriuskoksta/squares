import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {PointList} from './point-list.model';
import {Point} from './point.model';
import {PointListResponse} from './point-list-response.model';
import {SquareListResponse} from './square-list-response.model';

@Injectable()
export class SquaresService {

  private headers;
  private squaresBackendUrl;

  private static handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  constructor(private http: Http) {
    this.squaresBackendUrl = 'http://localhost:5000/api';
    this.headers = new Headers({'Content-Type': 'application/json'});
  }

  getPointLists(): Promise<PointList[]> {
    return this.http.get(this.squaresBackendUrl + '/lists')
      .toPromise()
      .then(response => response.json() as PointList[])
      .catch(SquaresService.handleError);
  }

  cleanPointList(list: PointList): Promise<void> {
    return this.http.delete(this.squaresBackendUrl + '/lists/' + list.id + '/points/clear', {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(SquaresService.handleError);
  }

  deletePointList(list: PointList): Promise<void> {
    return this.http.delete(this.squaresBackendUrl + '/lists/' + list.id, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(SquaresService.handleError);
  }

  getPointList(listId: number): Promise<PointList> {
    return this.http.get(this.squaresBackendUrl + '/lists/' + listId, {headers: this.headers})
      .toPromise()
      .then(response => response.json() as PointList)
      .catch(SquaresService.handleError);
  }

  updatePointList(list: PointList): Promise<void>  {
    return this.http.put(this.squaresBackendUrl + '/lists/' + list.id, list)
      .toPromise()
      .then(() => null)
      .catch(SquaresService.handleError);
  }

  startNewList(name: string): Promise<void>  {
    return this.http.post(this.squaresBackendUrl + '/lists', {name: name})
      .toPromise()
      .then(() => null)
      .catch(SquaresService.handleError);
  }

  getPoints(list: PointList, currentPage: number, perPage: number): Promise<PointListResponse> {
    return this.http.get(
        this.squaresBackendUrl + '/lists/' + list.id + '/points?page=' + currentPage + '&perPage=' + perPage,
        {headers: this.headers}
      )
      .toPromise()
      .then(response => response.json() as PointListResponse)
      .catch(SquaresService.handleError);
  }

  deletePoint(list: PointList, point: Point): Promise<void> {
    return this.http.delete(this.squaresBackendUrl + '/lists/' + list.id + '/points/' + point.x + ':' + point.y, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(SquaresService.handleError);
  }

  getSquares(list: PointList, currentPage: number, perPage: number): Promise<SquareListResponse> {
    return this.http.get(
        this.squaresBackendUrl + '/lists/' + list.id + '/squares?page=' + currentPage + '&perPage=' + perPage,
        {headers: this.headers}
      )
      .toPromise()
      .then(response => response.json() as SquareListResponse)
      .catch(SquaresService.handleError);
  }

  addNewPoint(list: PointList, point: Point): Promise<any>  {
    return this.http.post(this.squaresBackendUrl + '/lists/' + list.id + '/points', point)
      .toPromise()
      .then(response => response)
      .catch(error => {
        if (error.status === 422) {
          return error.json();
        } else if (error.status === 404) {
          return ['List not found'];
        }
        return SquaresService.handleError(error);
      });
  }

}

