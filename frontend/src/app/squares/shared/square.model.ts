import {Point} from './point.model';
export class Square {
  constructor(
    public corners: Point[]
  ) {}
}
