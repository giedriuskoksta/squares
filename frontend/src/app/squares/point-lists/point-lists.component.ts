import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {PointList} from '../shared/point-list.model';
import {SquaresService} from '../shared/squares.service';

@Component({
  selector: 'squares-point-lists',
  templateUrl: './point-lists.component.html',
  styleUrls: ['./point-lists.component.scss']
})

export class PointListsComponent implements OnInit {

  pointLists: PointList[];
  renamingPointList: PointList;

  public constructor(
    private titleService: Title,
    private squaresService: SquaresService
  ) {}

  ngOnInit() {
    this.titleService.setTitle('Lists');
    this.getPointLists();
  }

  getPointLists(): void {
    this.squaresService.getPointLists().then(pointLists => this.pointLists = pointLists);
  }

  rename(pointListToRename: PointList): void {
    this.renamingPointList = pointListToRename;
  }

  clean(pointListToClean: PointList): void {
    this.squaresService
      .cleanPointList(pointListToClean)
      .then(() => {
        pointListToClean.points = [];
      });

  }

  delete(pointListToDelete: PointList): void {
    this.squaresService
      .deletePointList(pointListToDelete)
      .then(() => {
        this.pointLists = this.pointLists.filter(pl => pl !== pointListToDelete);
      });

  }

  onPointListAdded(): void {
    this.getPointLists();
  }

  onRenameDone(): void {
    this.renamingPointList = null;
    this.getPointLists();
  }
}

