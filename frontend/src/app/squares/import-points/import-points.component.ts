import {Component, Input, EventEmitter, Output, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PointList} from '../shared/point-list.model';
import {FileUploader, FileItem} from 'ng2-file-upload';

@Component({
  selector: 'squares-import-points-content',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Import points</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Dismiss')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div class="form-group {{ validation.length > 0 ? 'has-danger' : '' }}">
        <input type="file" name="importPoints" class="form-control-file" ng2FileSelect [uploader]="uploader" />
        <div ng-if="validation == ''" class="form-control-feedback">
          <p *ngFor="let message of validation">{{ message }}</p>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-success btn-s"
        (click)="importFile()"
        [disabled]="!uploader.getNotUploadedItems().length">Upload</button>
      <button type="button" class="btn btn-danger btn-s" (click)="activeModal.close()">Cancel</button>
    </div>
  `,
})

export class ImportPointsContent implements OnInit  {
  @Input() pointList: PointList;
  @Input() notifyImported: EventEmitter<string>;
  public uploader: FileUploader;
  private validation: string[] = [];

  constructor(
    public activeModal: NgbActiveModal) {}

  ngOnInit(): void {
    this.uploader = new FileUploader({
        url: 'http://localhost:5000/api/lists/' + this.pointList.id + '/points/import',
        itemAlias: 'importPoints'
    });
  }

  importFile(): void {
    this.validation = [];
    this.uploader.uploadAll();
    this.uploader.onBeforeUploadItem = (item: FileItem) => {
      this.validation = [];
    };
    this.uploader.onSuccessItem = (item: FileItem, response: string, status: number) => {
      this.validation = JSON.parse(response);
      this.notifyImported.emit('POINTS_IMPORTED');
      if (this.validation.length === 0) {
        this.activeModal.close(true);
      }
    };
  }
}

@Component({
  selector: 'squares-import-points',
  templateUrl: './import-points.component.html'
})
export class ImportPointsComponent {
  @Input() pointList: PointList;
  @Output() notifyImported: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private modalService: NgbModal
  ) {}

  open(): void {
    const modalRef = this.modalService.open(ImportPointsContent);
    modalRef.componentInstance.pointList = this.pointList;
    modalRef.componentInstance.notifyImported = this.notifyImported;
  }
}
