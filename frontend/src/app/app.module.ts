import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {NavigationComponent} from './navigation.component';
import {AppRoutingModule} from './app-routing.module';
import {SquaresModule} from './squares/squares.module';
import {HttpModule} from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    SquaresModule
  ],
  providers: [
    Title
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
