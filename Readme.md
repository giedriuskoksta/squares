## To start app
1. Make sure you have docker and docker-compose installed 
2. run `docker-compose up --build`  
3. Navigate to http://localhost:4200

## To run tests
1. `cd backend`  
2. `./gradlew test`
