package com.devbridge.squares.backend.domain;

import com.devbridge.squares.backend.domain.exceptions.PointIsOutOfRangeException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class PointTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void itDoesNotAllowXCoordinateToBeMoreThan5000() throws Exception  {
        exception.expect(PointIsOutOfRangeException.class);
        Point.fromCoordinates(5001, 1);
    }

    @Test
    public void itDoesNotAllowXCoordinateToBeLessThanNegative5000() throws Exception  {
        exception.expect(PointIsOutOfRangeException.class);
        Point.fromCoordinates(-5001, 1);
    }

    @Test
    public void itDoesNotAllowYCoordinateToBeMoreThan5000() throws Exception  {
        exception.expect(PointIsOutOfRangeException.class);
        Point.fromCoordinates(1, 5001);
    }

    @Test
    public void itDoesNotAllowYCoordinateToBeLessThanNegative5000() throws Exception  {
        exception.expect(PointIsOutOfRangeException.class);
        Point.fromCoordinates(1, -5001);
    }

    @Test
    public void itAllowsToCreatePointsWhichAreValidAndInRange() throws Exception  {
        Point point = Point.fromCoordinates(5000, 5000);
        Point point2 = Point.fromCoordinates(-5000, -5000);
        assertEquals("Point coordinate x", point.getX(), 5000);
        assertEquals("Point coordinate y", point.getY(), 5000);
        assertEquals("Point 2 coordinate x", point2.getX(), -5000);
        assertEquals("Point 2 coordinate y", point2.getY(), -5000);
    }

    @Test
    public void itCanBeBuildFromKey() throws Exception  {
        Point testPoint = Point.fromKey("1:2");
        assertNotNull(testPoint);
        assertEquals("Point x", testPoint.getX(), 1);
        assertEquals("Point y", testPoint.getY(), 2);
    }

    @Test
    public void itCanBeBuildFromLine() throws Exception  {
        Point testPoint = Point.fromLine("1 2");
        assertNotNull(testPoint);
        assertEquals("Point x", testPoint.getX(), 1);
        assertEquals("Point y", testPoint.getY(), 2);
    }

    @Test
    public void itHasHey() throws Exception  {
        Point testPoint = Point.fromCoordinates(1, 2);
        assertNotNull(testPoint);
        assertEquals("Point key", testPoint.getKey(), "1:2");
    }

    @Test
    public void itCorrectlyAssumesIfPointsAreEqual() throws Exception  {
        Point testPoint = Point.fromCoordinates(1, 2);
        Point testPointTwo = Point.fromCoordinates(1, 2);
        assertTrue(testPoint.equals(testPointTwo));
        Point testPointThree = Point.fromCoordinates(1, 2);
        Point testPointFour = Point.fromCoordinates(1, 1);
        assertFalse(testPointThree.equals(testPointFour));
    }
}