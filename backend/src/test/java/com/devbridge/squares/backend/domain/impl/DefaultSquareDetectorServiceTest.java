package com.devbridge.squares.backend.domain.impl;

import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.Square;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertTrue;

public class DefaultSquareDetectorServiceTest {
    @Test
    public void itDetectSquare() throws Exception {
        PointList testPointList = new PointList("Test");
        testPointList.addPoint(0, 0);
        testPointList.addPoint(1, 0);
        testPointList.addPoint(0, 1);
        testPointList.addPoint(1, 1);

        Set<Square> squares = new DefaultSquareDetectorService().detect(testPointList);
        assertTrue(squares.size() == 1);
    }

    @Test
    public void itDoesNotDetectRectangle() throws Exception {
        PointList testPointList = new PointList("Test Two");
        testPointList.addPoint(0, 0);
        testPointList.addPoint(2, 0);
        testPointList.addPoint(0, 1);
        testPointList.addPoint(2, 1);

        Set<Square> squares = new DefaultSquareDetectorService().detect(testPointList);
        assertTrue(squares.size() == 0);
    }

    @Test
    public void itDetectsSmallSquareInsideBig() throws Exception {
        PointList testPointList = new PointList("Test Three");
        testPointList.addPoint(1, 1);
        testPointList.addPoint(2, 1);
        testPointList.addPoint(1, 2);
        testPointList.addPoint(2, 2);
        testPointList.addPoint(0, 0);
        testPointList.addPoint(4, 4);
        testPointList.addPoint(0, 4);
        testPointList.addPoint(4, 0);

        Set<Square> squares = new DefaultSquareDetectorService().detect(testPointList);
        assertTrue(squares.size() == 2);
    }

    @Test
    public void itDetectsSquaresWithSameSide() throws Exception {
        PointList testPointList = new PointList("Test Three");
        testPointList.addPoint(-4, 0);
        testPointList.addPoint(-4, 4);
        testPointList.addPoint(0, 0);
        testPointList.addPoint(4, 4);
        testPointList.addPoint(0, 4);
        testPointList.addPoint(4, 0);

        Set<Square> squares = new DefaultSquareDetectorService().detect(testPointList);
        assertTrue(squares.size() == 2);
    }
}