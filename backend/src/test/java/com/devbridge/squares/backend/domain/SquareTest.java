package com.devbridge.squares.backend.domain;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SquareTest {
    @Test
    public void itCorrectlyAssumesEqualSquares() throws Exception {
        Square testSquare = new Square(
                Point.fromCoordinates(1, 1),
                Point.fromCoordinates(0, 1),
                Point.fromCoordinates(1, 0),
                Point.fromCoordinates(0, 0)
        );
        Square testSquareTwo = new Square(
                Point.fromCoordinates(1, 0),
                Point.fromCoordinates(0, 1),
                Point.fromCoordinates(0, 0),
                Point.fromCoordinates(1, 1)
        );
        assertTrue(testSquare.equals(testSquareTwo));
        Square testSquareThree = new Square(
                Point.fromCoordinates(1, 1),
                Point.fromCoordinates(0, 1),
                Point.fromCoordinates(1, 0),
                Point.fromCoordinates(0, 0)
        );
        Square testSquareFour = new Square(
                Point.fromCoordinates(1, 0),
                Point.fromCoordinates(0, 1),
                Point.fromCoordinates(0, 0),
                Point.fromCoordinates(1, 2)
        );
        assertFalse(testSquareThree.equals(testSquareFour));
    }
}