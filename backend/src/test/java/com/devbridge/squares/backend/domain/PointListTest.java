package com.devbridge.squares.backend.domain;

import com.devbridge.squares.backend.domain.exceptions.DuplicatePointException;
import com.devbridge.squares.backend.domain.exceptions.PointDoesNotExistException;
import com.devbridge.squares.backend.domain.exceptions.TooManyPointsInListException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class PointListTest {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void itAllowsPointsToBeAddedToList() throws Exception {
        PointList testList = new PointList("test list");
        testList.addPoint(1, 1);
        assertEquals("Number of points", 1, testList.getPoints().size());
    }

    @Test
    public void itForbidsToAddDuplicatePoints() throws Exception  {
        exception.expect(DuplicatePointException.class);
        PointList testList = new PointList("test list");
        testList.addPoint(1, 1);
        testList.addPoint(1, 1);
    }

    @Test
    public void itForbidsAddingMoreThan10000Points() throws Exception  {
        exception.expect(TooManyPointsInListException.class);
        PointList testList = new PointList("test list");
        for(int i = -5000; i <= 5000; i++) {
            testList.addPoint(i, i);
        }
    }

    @Test
    public void itChangesNameOfList() throws Exception  {
        PointList testList = new PointList("test list");
        testList.changeName("New Name");
        assertEquals("Changed name","New Name", testList.getName());
    }

    @Test
    public void itCanCleanList() throws Exception  {
        PointList testList = new PointList("test list");
        testList.addPoint(1, 1);
        assertEquals("Added point", 1, testList.getPoints().size());
        testList.clearPoints();
        assertEquals("Cleared points", 0, testList.getPoints().size());
    }

    @Test
    public void itCanRemovePoints() throws Exception  {
        PointList testList = new PointList("test list");
        testList.addPoint(1, 1);
        assertEquals("Added point to remove", 1, testList.getPoints().size());
        testList.removePoint(Point.fromKey("1:1"));
        assertEquals("Removed point", 0, testList.getPoints().size());
    }

    @Test
    public void itFailesWhenTryingToRemoveMissingPoint() throws Exception  {
        exception.expect(PointDoesNotExistException.class);
        PointList testList = new PointList("test list");
        testList.removePoint(Point.fromKey("1:1"));
    }
}