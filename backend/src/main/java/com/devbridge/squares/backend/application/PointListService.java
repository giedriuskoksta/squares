package com.devbridge.squares.backend.application;

import com.devbridge.squares.backend.application.exceptions.ListNotFoundException;
import com.devbridge.squares.backend.domain.Point;
import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.PointListRepository;
import com.devbridge.squares.backend.domain.exceptions.DomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class PointListService {

    private final static Logger logger = LoggerFactory.getLogger(PointListService.class);
    private final PointListRepository pointListRepository;

    public PointListService(PointListRepository pointListRepository) {
        this.pointListRepository = pointListRepository;
    }

    public PointList startList(String name) {
        if (name == null || name.length() == 0) {
            List<PointList> list = this.pointListRepository.findAll();
            name = "New List " + list.size();
        }

        PointList newPointList = new PointList(getUniqueName(name));
        this.pointListRepository.save(newPointList);

        return newPointList;
    }

    public void updateList(int listId, String name) throws ListNotFoundException
    {
        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            throw new ListNotFoundException("List ID:" + listId + " not found");
        }

        PointList pointListWithSameName = this.pointListRepository.findByName(name);
        if (pointListWithSameName != null && pointListWithSameName.getId() != pointList.getId()) {
            this.pointListRepository.delete(pointListWithSameName);
        }

        pointList.changeName(name);

        this.pointListRepository.save(pointList);
    }

    public List<String> addPoints(int listId, Point point, List<String> notifications) throws ListNotFoundException {
        List<Point> points = new ArrayList<>();
        points.add(point);
        return this.addPoints(listId, points, notifications);
    }

    public List<String> addPoints(int listId, List<Point> points, List<String> notifications) throws ListNotFoundException {
        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            throw new ListNotFoundException("List ID:" + listId + " not found");
        }

        points.forEach(p -> this.addPoint(pointList, p, notifications));

        this.pointListRepository.save(pointList);

        return notifications;
    }

    private String getUniqueName(String name) {
        boolean isUnique;
        do {
            PointList pointList = this.pointListRepository.findByName(name);
            if (pointList != null) {
                name = pointList.getName() + " Copy";
                isUnique = false;
            } else {
                isUnique = true;
            }
        } while (!isUnique);
        return name;
    }

    private void addPoint(PointList pointList, Point point, List<String> notifications) {
        try {
            pointList.addPoint(point.getX(), point.getY());
        } catch (DomainException e) {
            notifications.add(e.getMessage());
            logger.error("Could not add point. {}", e.getMessage());
        }
    }
}
