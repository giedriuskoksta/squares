package com.devbridge.squares.backend.application.exceptions;

public class ListAlreadyExistException extends ApplicationException {
    public ListAlreadyExistException(String message) {
        super(message);
    }
}
