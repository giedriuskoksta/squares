package com.devbridge.squares.backend.presentation;

import com.devbridge.squares.backend.application.PointListService;
import com.devbridge.squares.backend.application.exceptions.ListNotFoundException;
import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.PointListRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ListsController {

    private final static Logger logger = LoggerFactory.getLogger(ListsController.class);
    private final PointListService pointListService;
    private final PointListRepository pointListRepository;

    @Autowired
    public ListsController(PointListService pointListService, PointListRepository pointListRepository) {
        this.pointListService = pointListService;
        this.pointListRepository = pointListRepository;
    }

    @GetMapping(value = "/lists", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll() {

        List<PointList> list = this.pointListRepository.findAll();

        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping(value = "/lists", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody PointList pointListRequest) {

        PointList newPointList = this.pointListService.startList(pointListRequest.getName());

        return new ResponseEntity<>(newPointList, HttpStatus.CREATED);

    }

    @GetMapping(value = "/lists/{listId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity get(@PathVariable int listId) {

        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(pointList, HttpStatus.OK);
    }

    @PutMapping(value = "/lists/{listId}")
    public ResponseEntity update(@PathVariable int listId, @RequestBody PointList pointListRequest) {

        try {
            Assert.hasLength(pointListRequest.getName(), "Empty list name");
            this.pointListService.updateList(listId, pointListRequest.getName());

            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (IllegalArgumentException e) {
            logger.debug("Invalid list name. {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (ListNotFoundException e) {
            logger.debug("Error finding list. {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/lists/{listId}")
    public ResponseEntity delete(@PathVariable int listId) {

        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        this.pointListRepository.delete(pointList);

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }
}
