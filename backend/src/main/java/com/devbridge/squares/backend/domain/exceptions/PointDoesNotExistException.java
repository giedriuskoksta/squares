package com.devbridge.squares.backend.domain.exceptions;

public class PointDoesNotExistException extends DomainException {
    public PointDoesNotExistException(String message) {
        super(message);
    }
}
