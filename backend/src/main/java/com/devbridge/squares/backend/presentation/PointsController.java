package com.devbridge.squares.backend.presentation;

import com.devbridge.squares.backend.application.ImportFileParser;
import com.devbridge.squares.backend.application.PointListService;
import com.devbridge.squares.backend.application.exceptions.ListNotFoundException;
import com.devbridge.squares.backend.domain.Point;
import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.PointListRepository;
import com.devbridge.squares.backend.presentation.responses.ListPagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path="/lists/{listId}")
public class PointsController {

    private final static Logger logger = LoggerFactory.getLogger(PointsController.class);
    private final PointListService pointListService;
    private final PointListRepository pointListRepository;

    @Autowired
    public PointsController(PointListService pointListService, PointListRepository pointListRepository) {
        this.pointListService = pointListService;
        this.pointListRepository = pointListRepository;
    }
    @GetMapping(value = "/points", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPoints(@PathVariable int listId,
                                    @RequestParam(name = "page", required=false, defaultValue = "1") String requestedPage,
                                    @RequestParam(name = "perPage", required=false, defaultValue = "5") String requestedPerPage) {

        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new ListPagination<List<Point>>().paginate(pointList.getPoints(), requestedPage, requestedPerPage), HttpStatus.OK);
    }

    @PostMapping(value = "/points", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity newPoint(@PathVariable int listId, @RequestBody Point point) {

        try {

            List<String> notificationMessages = new ArrayList<>();
            notificationMessages = this.pointListService.addPoints(listId, point, notificationMessages);

            if (notificationMessages.size() > 0) {
                return new ResponseEntity<>(notificationMessages, HttpStatus.UNPROCESSABLE_ENTITY);
            }
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        } catch (ListNotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(value = "/points/import", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity importPoints(@PathVariable int listId, @RequestParam("importPoints") MultipartFile file) {
        try {
            if (file.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.UNPROCESSABLE_ENTITY);
            }

            ImportFileParser parser = new ImportFileParser(file).parse();
            List<String> notificationMessages = parser.getNotifications();
            notificationMessages = this.pointListService.addPoints(listId, parser.getPoints(), notificationMessages);

            return new ResponseEntity<>(notificationMessages, HttpStatus.OK);

        } catch (ListNotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/points/clear")
    public ResponseEntity clearPointList(@PathVariable int listId) {

        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        pointList.clearPoints();
        this.pointListRepository.save(pointList);

        return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping(value = "/points/{pointKey}")
    public ResponseEntity deletePoint(@PathVariable int listId, @PathVariable String pointKey) {

        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        try {
            pointList.removePoint(Point.fromKey(pointKey));
            this.pointListRepository.save(pointList);

            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            logger.debug("Point could not be deleted. {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
