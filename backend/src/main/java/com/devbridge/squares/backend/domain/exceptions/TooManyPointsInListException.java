package com.devbridge.squares.backend.domain.exceptions;

public class TooManyPointsInListException extends DomainException {
    public TooManyPointsInListException(String message) {
        super(message);
    }
}
