package com.devbridge.squares.backend.domain.exceptions;

public class DuplicatePointException extends DomainException {
    public DuplicatePointException(String message) {
        super(message);
    }
}
