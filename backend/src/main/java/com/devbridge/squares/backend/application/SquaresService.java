package com.devbridge.squares.backend.application;

import com.devbridge.squares.backend.application.exceptions.ListNotFoundException;
import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.PointListRepository;
import com.devbridge.squares.backend.domain.Square;
import com.devbridge.squares.backend.domain.SquareDetectorService;

import java.util.Set;

public class SquaresService {

    private final PointListRepository pointListRepository;
    private final SquareDetectorService squareDetectorService;

    public SquaresService(SquareDetectorService squareDetectorService, PointListRepository pointListRepository) {
        this.squareDetectorService = squareDetectorService;
        this.pointListRepository = pointListRepository;
    }

    public Set<Square> getDetectedSquares(int listId) throws ListNotFoundException {
        PointList pointList = this.pointListRepository.findOne(listId);
        if (pointList == null) {
            throw new ListNotFoundException("List not found ID:" + listId);
        }
        return this.squareDetectorService.detect(pointList);
    }

}
