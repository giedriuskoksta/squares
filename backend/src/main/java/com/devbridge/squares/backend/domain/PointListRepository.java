package com.devbridge.squares.backend.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PointListRepository extends JpaRepository<PointList, Integer> {

    PointList findByName(String name);
}
