package com.devbridge.squares.backend.presentation;

import com.devbridge.squares.backend.application.SquaresService;
import com.devbridge.squares.backend.application.exceptions.ListNotFoundException;
import com.devbridge.squares.backend.domain.Square;
import com.devbridge.squares.backend.presentation.responses.ListPagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(path="/lists/{listId}")
public class SquaresController {

    private final SquaresService squaresService;

    @Autowired
    public SquaresController(SquaresService squaresService) {
        this.squaresService = squaresService;
    }

    @GetMapping(value = "/squares", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getSquaresForPoints(@PathVariable int listId,
                                              @RequestParam(name = "page", required=false, defaultValue = "1") String requestedPage,
                                              @RequestParam(name = "perPage", required=false, defaultValue = "5") String requestedPerPage) {

        try {
            Set<Square> squares = this.squaresService.getDetectedSquares(listId);

            return new ResponseEntity<>(new ListPagination<Set<Square>>().paginate(squares, requestedPage, requestedPerPage), HttpStatus.OK);
        } catch (ListNotFoundException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
