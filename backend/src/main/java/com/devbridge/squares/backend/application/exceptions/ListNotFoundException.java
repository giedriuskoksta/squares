package com.devbridge.squares.backend.application.exceptions;

public class ListNotFoundException extends ApplicationException {
    public ListNotFoundException(String message) {
        super(message);
    }
}
