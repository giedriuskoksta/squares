package com.devbridge.squares.backend.domain.impl;

import com.devbridge.squares.backend.domain.Point;
import com.devbridge.squares.backend.domain.PointList;
import com.devbridge.squares.backend.domain.Square;
import com.devbridge.squares.backend.domain.SquareDetectorService;
import com.devbridge.squares.backend.domain.exceptions.DomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class DefaultSquareDetectorService implements SquareDetectorService {

    private final static Logger logger = LoggerFactory.getLogger(DefaultSquareDetectorService.class);
    @Override
    public Set<Square> detect(PointList pointList) {

        //Group points to brackets by X coordinate
        Map<Integer, List<Point>> grouped = pointList.getPoints().stream().collect(Collectors.groupingBy(Point::getX));

        Set<Square> squareList = new HashSet<>();

        grouped.forEach((groupKey, group) -> group
                .stream()
                //generate all possible pairs in group
                .flatMap(p1 -> group.stream().filter(p2 -> !p1.equals(p2)).map(p2 -> new HashSet<>(Arrays.asList(p1, p2))))
                //Remove duplicate points Eg. (1, 2) (2, 1) ==  (2, 1) (1, 2)
                .distinct()
                .forEach(p -> {
                    //Unpacking Set
                    Object[] points = p.toArray();

                    //Check for initial square
                    Square square = this.findSquare(grouped, (Point) points[0], (Point) points[1], false);
                    if (square != null) {
                        squareList.add(square);
                    }

                    //Check for reversed square
                    Square square2 = this.findSquare(grouped, (Point) points[0], (Point) points[1], true);
                    if (square2 != null) {
                        squareList.add(square2);
                    }
                })
        );
        return squareList;
    }

    private Square findSquare(Map<Integer, List<Point>> grouped, Point p1, Point p2, boolean isReversed) {

        int distance = isReversed ? -1*p1.distanceFrom(p2) : p1.distanceFrom(p2);

        if (!grouped.containsKey(p1.getX()+distance)) {
            return null;
        }
        try {
            Point p3 = Point.fromCoordinates(p1.getX()+distance, p1.getY());
            Point p4 = Point.fromCoordinates(p2.getX()+distance, p2.getY());
            if (grouped.get(p1.getX()+distance).contains(p3) && grouped.get(p1.getX()+distance).contains(p4)) {
                return new Square(p1, p2, p3, p4);
            }
        } catch (DomainException e) {
            logger.error("Invalid point data");
        }
        return null;
    }
}
