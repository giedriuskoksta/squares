package com.devbridge.squares.backend.presentation.responses;

import lombok.Getter;

import java.util.Collection;

public class ListResponse<L extends Collection> {
    @Getter
    private int total;

    @Getter
    private L items;

    ListResponse(int total, L items) {
        this.total = total;
        this.items = items;
    }
}
