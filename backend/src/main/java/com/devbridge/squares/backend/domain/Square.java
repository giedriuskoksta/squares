package com.devbridge.squares.backend.domain;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

public class Square {
    @Getter private Set<Point> corners = new HashSet<>();

    public Square() {}

    public Square(Point cornerA, Point cornerB, Point cornerC, Point cornerD) {
        this.corners.add(cornerA);
        this.corners.add(cornerB);
        this.corners.add(cornerC);
        this.corners.add(cornerD);
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof Square && this.corners.equals(((Square) object).getCorners());
    }

    @Override
    public int hashCode() {
        return this.corners.stream().mapToInt(c -> c.getX() + c.getY()).sum();
    }
}
