package com.devbridge.squares.backend.domain;

import java.util.Set;

public interface SquareDetectorService {
    Set<Square> detect(PointList pointList);
}
