package com.devbridge.squares.backend.domain.exceptions;

public class InvalidPointKeyException extends DomainException {
    public InvalidPointKeyException(String message) {
        super(message);
    }
}
