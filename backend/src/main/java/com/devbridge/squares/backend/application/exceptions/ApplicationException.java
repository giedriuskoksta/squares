package com.devbridge.squares.backend.application.exceptions;

public class ApplicationException extends Exception {
    public ApplicationException(String message) {
        super(message);
    }
}
