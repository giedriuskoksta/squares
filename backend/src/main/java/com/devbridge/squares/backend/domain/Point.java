package com.devbridge.squares.backend.domain;

import com.devbridge.squares.backend.domain.exceptions.DomainException;
import com.devbridge.squares.backend.domain.exceptions.InvalidPointKeyException;
import com.devbridge.squares.backend.domain.exceptions.PointIsOutOfRangeException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@Table(name="Points")
public class Point implements Serializable {

    private static final int MAX_COORDINATE = 5000;
    private static final int MIN_COORDINATE = -5000;

    @Getter
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @JsonIgnore
    private int id;

    @Getter private int x;
    @Getter private int y;

    public Point(){}

    private Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point fromCoordinates(int x, int y) throws DomainException {
        if (isOutOfRange(x) || isOutOfRange(y)) {
            throw new PointIsOutOfRangeException("Point (" + x + ", " + y + ") is out of range. Min " + MIN_COORDINATE + ", Max " + MAX_COORDINATE);
        }
        return new Point(x, y);
    }

    public static Point fromKey(String key) throws DomainException {
        PointCoordinateExtractor c = new PointCoordinateExtractor(key, "(-?\\d+):(-?\\d+)").invoke();
        return Point.fromCoordinates(c.getX(), c.getY());
    }

    public static Point fromLine(String key) throws DomainException {
        PointCoordinateExtractor c = new PointCoordinateExtractor(key, "(-?\\d+) (-?\\d+)").invoke();
        return Point.fromCoordinates(c.getX(), c.getY());
    }

    public String getKey() {
        return this.x + ":" + this.y;
    }

    public int distanceFrom(Point point) {
        return (int) Math.sqrt(Math.pow(this.x - point.getX(), 2) + Math.pow(this.y - point.getY(), 2));
    }

    @Override
    public boolean equals(Object object) {
        return object instanceof Point && this.x == ((Point) object).getX() && this.y == ((Point) object).getY();
    }

    @Override
    public int hashCode() {
        return Math.abs(this.x) + Math.abs(this.y);
    }

    private static boolean isOutOfRange(int coordinate) {
        return MIN_COORDINATE > coordinate || coordinate > MAX_COORDINATE;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    private static class PointCoordinateExtractor {
        private String key;
        private String keyPattern;
        @Getter private int x;
        @Getter private int y;

        PointCoordinateExtractor(String key, String keyPattern) {
            this.key = key;
            this.keyPattern = keyPattern;
        }

        PointCoordinateExtractor invoke() throws InvalidPointKeyException {
            Pattern pattern = Pattern.compile(keyPattern);
            Matcher matcher = pattern.matcher(key);
            if (!matcher.matches()) {
                throw new InvalidPointKeyException("Point key is invalid. " + key);
            }
            x = Integer.parseInt(matcher.group(1));
            y = Integer.parseInt(matcher.group(2));
            return this;
        }
    }
}
