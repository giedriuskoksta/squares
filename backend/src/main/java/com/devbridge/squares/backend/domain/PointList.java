package com.devbridge.squares.backend.domain;

import com.devbridge.squares.backend.domain.exceptions.DomainException;
import com.devbridge.squares.backend.domain.exceptions.DuplicatePointException;
import com.devbridge.squares.backend.domain.exceptions.PointDoesNotExistException;
import com.devbridge.squares.backend.domain.exceptions.TooManyPointsInListException;
import lombok.Getter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="PointLists")
public class PointList {

    private static final int MAX_POINTS_IN_LIST = 10000;

    @Getter
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Getter private String name;

    @Getter
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Point> points;

    public PointList() {}

    public PointList(String name) {
        this.name = name;
        this.points = new ArrayList<>();
    }

    public void changeName(String name) {
        if (!this.name.equals(name)) {
            this.name = name;
        }
    }

    public void clearPoints() {
        this.points.clear();
    }

    public void addPoint(int x, int y) throws DomainException {
        if (this.points.size() >= MAX_POINTS_IN_LIST) {
            throw new TooManyPointsInListException("Too many points in list");
        }

        Point point = Point.fromCoordinates(x, y);
        if (this.points.contains(point)) {
            throw new DuplicatePointException("Duplicate point in list: " + point);
        }

        this.points.add(point);
    }

    public void removePoint(Point point) throws PointDoesNotExistException {
        if (!this.points.contains(point)) {
            throw new PointDoesNotExistException("Point does not exist in list: " + point);
        }
        this.points.remove(point);
    }
}
