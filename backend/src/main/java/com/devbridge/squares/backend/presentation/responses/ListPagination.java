package com.devbridge.squares.backend.presentation.responses;

import java.util.Collection;
import java.util.stream.Collectors;

public class ListPagination<L extends Collection> {

    @SuppressWarnings("unchecked")
    public ListResponse paginate(L items, String requestedPage, String requestedPerPage) {
        int page = Integer.valueOf(requestedPage);
        page = page > 0 ? page : 1;
        int perPage = Integer.valueOf(requestedPerPage);
        perPage = perPage > 0 ? perPage : 5;
        L filteredList = (L) items.stream()
                .skip(page*perPage - perPage)
                .limit(perPage)
                .collect(Collectors.toList());
        return new ListResponse<>(items.size(), filteredList);
    }
}
