package com.devbridge.squares.backend.configuration;

import com.devbridge.squares.backend.application.PointListService;
import com.devbridge.squares.backend.application.SquaresService;
import com.devbridge.squares.backend.domain.PointListRepository;
import com.devbridge.squares.backend.domain.SquareDetectorService;
import com.devbridge.squares.backend.domain.impl.DefaultSquareDetectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfig {

    private PointListRepository pointListRepository;

    @Autowired
    void pointListRepository(PointListRepository pointListRepository) {
        this.pointListRepository = pointListRepository;
    }

    @Bean
    PointListService pointListService() {
        return new PointListService(this.pointListRepository);
    }

    @Bean
    SquaresService squaresService() {
        return new SquaresService(this.squareDetectorService(), this.pointListRepository);
    }

    @Bean
    SquareDetectorService squareDetectorService() {
        return new DefaultSquareDetectorService();
    }
}
