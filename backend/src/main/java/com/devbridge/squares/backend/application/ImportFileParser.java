package com.devbridge.squares.backend.application;

import com.devbridge.squares.backend.domain.Point;
import com.devbridge.squares.backend.domain.exceptions.DomainException;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ImportFileParser {

    private final static Logger logger = LoggerFactory.getLogger(ImportFileParser.class);
    private MultipartFile file;
    @Getter private List<Point> points = new ArrayList<>();
    @Getter private List<String> notifications = new ArrayList<>();

    public ImportFileParser(MultipartFile file) {
       this.file = file;
    }

    public ImportFileParser parse() {
        try {
            BufferedReader buf = new BufferedReader(new InputStreamReader(file.getInputStream()));
            String line = buf.readLine();
            while(line != null){
                try {
                    this.points.add(Point.fromLine(line));
                } catch (DomainException e) {
                    this.notifications.add(e.getMessage());
                    logger.debug("{}", e.getMessage());
                }
                line = buf.readLine();
            }
        } catch (IOException e) {
            logger.error("{}", e.getMessage());
            this.notifications.add("Could not parse file");
        }
        return this;
    }
}
