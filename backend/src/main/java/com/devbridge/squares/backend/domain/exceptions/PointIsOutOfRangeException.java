package com.devbridge.squares.backend.domain.exceptions;

public class PointIsOutOfRangeException extends DomainException {
    public PointIsOutOfRangeException(String message) {
        super(message);
    }
}
